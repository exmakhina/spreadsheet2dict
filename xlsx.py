#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# spreadsheet2dict - Excel XML formats (.xlsx)
# SPDX-FileCopyrightText: 2021 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT

import logging

from openpyxl import load_workbook


logger = logging.getLogger(__name__)


def read(path: str) -> dict:
	"""
	Get named ranges from an Excel spreadsheet
	"""

	wb = load_workbook(filename=path)

	defined_names = wb.defined_names

	sheet_params = dict()

	for param in defined_names.definedName:
		name = param.name
		logger.debug("Looking at defined range %s @ %s", name, param.value)

		if param.value is None:
			logger.warning("Skipping empty named range: %s", name)
			continue

		for title, coords in param.destinations:
			sheet = wb[title]
			val = sheet[coords]
			logger.debug("-> Named range is %s/%s, value %s",
			 title, coords, val)

			if isinstance(val, tuple):
				param_val = [ [x.value for x in v] for v in val ]
			else:
				param_val = [[val.value]]

			logger.debug("Taking from spreadsheet %s = %s", name, param_val)
			sheet_params[name] = param_val

	return sheet_params


.. -*- coding: utf-8; indent-tabs-mode:nil; -*-
.. SPDX-FileCopyrightText: 2021 Jérôme Carretero <cJ@zougloub.eu>
.. SPDX-License-Identifier: MIT

################
spreadsheet2dict
################

A spreadsheet can be used as a dictionary, with named ranges providing
a mapping between string keys and 2D array values.

This library provides read methods that construct dictionaries from
spreadsheets.

License: MIT (see SPDX headers & LICENSES/).


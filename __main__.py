#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# spreadsheet2dict - cli
# SPDX-FileCopyrightText: 2021 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT

import sys, logging

from . import read


logger = logging.getLogger()


def main():
	import argparse
	parser = argparse.ArgumentParser(
	 description="Spreadsheet parser",
	)

	parser.add_argument("--log-level",
	 default="WARNING",
	 help="Logging level (eg. INFO, see Python logging docs)",
	)

	subparsers = parser.add_subparsers(
	 help='the command; type "%s COMMAND -h" for command-specific help' % sys.argv[0],
	 dest='command',
	)

	subp = subparsers.add_parser(
	 "yaml",
	 help="Read params from a spreadsheet, format as YAML",
	)

	subp.add_argument("input",
	 help="Path to input spreadsheet file containing ATP parameters",
	)

	def do_read(args):
		params = read(args.input)
		import yaml
		yaml.dump(params, sys.stdout)

	subp.set_defaults(func=do_read)


	args = parser.parse_args()

	logging.basicConfig(
	 level=getattr(logging, args.log_level),
	 format="%(levelname)s %(message)s"
	)

	try:
		import coloredlogs
		coloredlogs.install(level=getattr(logging, args.log_level), logger=logger)
	except ImportError:
		pass

	if getattr(args, 'func', None) is None:
		parser.print_help()
		raise SystemExit(1)
	else:
		args.func(args)
		raise SystemExit()


if __name__ == '__main__':
	main()

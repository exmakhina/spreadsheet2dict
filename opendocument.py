#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# spreadsheet2dict - OpenDocument formats (.ods, .fods)
# SPDX-FileCopyrightText: 2021 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT


import io, re
import logging
import decimal

import numpy as np
import lxml.etree

logger = logging.getLogger(__name__)


ns = {
 "office": "urn:oasis:names:tc:opendocument:xmlns:office:1.0",
 "table": "urn:oasis:names:tc:opendocument:xmlns:table:1.0",
 "text": "urn:oasis:names:tc:opendocument:xmlns:text:1.0",
}


def attr(e, n, x, default=None):
	return e.attrib.get("{{{}}}{}".format(ns[n], x), default)


def xmlt(x):
	data = lxml.etree.tostring(x,
	 #xml_declaration=True,
	 encoding='utf-8',
	 pretty_print=True,
	)
	return data


def ABC_to_x(x: str) -> int:
	v = 0
	for e in x:
		v = v * 26 + ord(e) - ord("A")
	return v


def A1_to_yx(x):
	#logger.debug("ABC %s", x)
	m = re.match(r"\$?(?P<col>[A-Z]+)\$?(?P<row>[0-9]+)", x)
	col_alpha = m.group("col")
	col = ABC_to_x(col_alpha)
	row = int(m.group("row"))-1
	return row, col


def get_tables(tree):
	tables = {}

	for table in tree.xpath("//table:table", namespaces=ns):
		cur_name = attr(table, "table", "name")
		#logger.debug(" Processing table %s", cur_name)

		rows = []

		idx_row = 0
		for row in table.xpath("./table:table-row", namespaces=ns):
			rep = int(attr(table, "table", "number-rows-repeated", 1))

			for idx_subrow in range(rep):

				#logger.debug("  Row %d", idx_row)

				cols = []
				idx_col = 0
				for cell in row.xpath("./table:table-cell", namespaces=ns):
					rep = int(attr(cell, "table", "number-columns-repeated", 1))

					for idx_subcol in range(rep):

						#logger.debug("   Col %d: %s", idx_col, xmlt(cell))

						if len(cell) == 0:
							value = None
						else:
							text = cell.xpath("text:p", namespaces=ns)[0].text

							fmt = attr(cell, "office", "value-type")
							if fmt == "float":
								text = attr(cell, "office", "value")
								try:
									value = int(text)
								except ValueError:
									value = decimal.Decimal(text)
							elif fmt == "string":
								if text is None:
									value = cell.xpath("string()")
								else:
									value = text
							elif fmt == "percentage":
								assert text.endswith("%")
								text = attr(cell, "office", "value")
								value = float(text)
							elif fmt == "date":
								text = attr(cell, "office", "date-value")
								value = text
							elif fmt is None:
								value = None
							else:
								raise NotImplementedError(fmt, text, xmlt(cell))

						#logger.debug("    add %s", value)

						cols += [value]
						idx_col += 1

				rows.append(cols)
				idx_row += 1

		nrows = len(rows)
		ncols = len(rows[0])
		logger.debug("Table has %d rows, %d cols", nrows, ncols)

		table = np.ndarray((nrows, ncols), dtype=object)

		for idx_row in range(nrows):
			for idx_col in range(ncols):
				try:
					table[idx_row, idx_col] = rows[idx_row][idx_col]
				except IndexError:
					table[idx_row, idx_col] = None

		tables[cur_name] = table

	return tables


def get_range(tables, name, base_cell, cell_range):
	logger.debug("Getting %s in %s/%s", name, base_cell, cell_range)
	if cell_range == "#REF!":
		raise ValueError(f"Bad range {name}")
	table0, c0 = base_cell.split(".")
	table1, r = cell_range.split(".", 1)
	if ":" in r:
		c1, c2 = r.split(":")
		if c2.startswith("."):
			c2 = c2[1:]
	else:
		c1 = r
		c2 = r

	table_name = table1[1:]
	c0 = A1_to_yx(c0)
	c1 = A1_to_yx(c1)
	c2 = A1_to_yx(c2)

	if c0 != (0,0):
		logger.warning("Base cell unexpected pos: %s", c0)

	logger.debug("Lookup %s %s %s %s", table_name, c0, c1, c2)

	y0, x0 = c1
	y1, x1 = c2
	y0, y1 = min(y0, y1), max(y0, y1)
	x0, x1 = min(x0, x1), max(x0, x1)

	logger.debug("Lookup %s %s-%sx%s-%s", table_name, y0, y1, x0, x1)

	ret = tables[table_name][y0:y1+1,x0:x1+1].tolist()

	logger.debug("-> %s", ret)
	return ret


def read(path: str) -> dict:
	"""
	Get params from an OpenDocument spreadsheet
	"""

	if path.endswith(".ods"):
		import zipfile
		with zipfile.ZipFile(path).open("content.xml") as fi:
			data = fi.read()
	else:
		with io.open(path, "rb") as fi:
			data = fi.read()

	out = {}
	tree = lxml.etree.XML(data)

	tables = get_tables(tree)

	namedranges = tree.xpath("//table:named-range", namespaces=ns)
	for nr in namedranges:
		name = attr(nr, "table", "name")
		base_cell = attr(nr, "table", "base-cell-address")
		cell_range = attr(nr, "table", "cell-range-address")

		logger.info("Processing %s", name)
		value = get_range(tables, name, base_cell, cell_range)

		out[name] = value

	return out

#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# spreadsheet2dict - "factory"
# SPDX-FileCopyrightText: 2021 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT

import logging

logger = logging.getLogger(__name__)


def read(path: str) -> dict:
	"""
	Get params from a spreadsheet
	"""

	if path.endswith(".xlsx"):
		from .xlsx import read
	elif path.endswith((".fods", ".ods")):
		from .opendocument import read
	else:
		raise NotImplementedError(path)

	return read(path)
